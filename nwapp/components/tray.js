'use strict';

const preferences = require('../utils/user-preferences');
const events = require('../utils/custom-events');

const CLIENT_TYPE = require('../utils/client-type');
const icon = require('../icons.json')[CLIENT_TYPE];

function CustomTray() {
  this.tray = new nw.Tray({
    icon: icon.disconnected,
    alticon: icon.selected,
    iconsAreTemplates: false,
  });

  events.on('user:signedOut', this.disconnect.bind(this));
  events.on('user:signedIn', this.connect.bind(this));
  events.on('traymenu:read', this.connect.bind(this));
  events.on('traymenu:unread', this.unread.bind(this));
}

CustomTray.prototype.setIcon = function (newIcon) {
  this.tray.icon = newIcon;
};

CustomTray.prototype.get = function () {
  return this.tray;
};
CustomTray.prototype.disconnect = function () {
  this.setIcon(icon.disconnected);
};
CustomTray.prototype.connect = function () {
  if (!preferences.token) return; // user is signed out
  this.setIcon(icon.connected);
};
CustomTray.prototype.unread = function () {
  this.setIcon(icon.unread);
};

module.exports = CustomTray;
